package org.aibles.util.exceptions.common;

import lombok.Getter;

@Getter
public enum ServiceError {
    DOCUMENT_NOT_FOUND_EXCEPTION(404001, "Document not found.");

    ServiceError(int errCode, String message) {
        this.errCode = errCode;
        this.message = message;
    }

    private final int errCode;
    private final String message;
}
