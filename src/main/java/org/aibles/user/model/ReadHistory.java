package org.aibles.user.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.aibles.document.model.Document;
import org.aibles.user.model.User;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "read_history")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ReadHistory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "read_history_id_seq")
    @SequenceGenerator(name = "read_history_id_seq",
        sequenceName = "pk_read_history_id_seq",
        allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "document_id")
    private Document document;
}
