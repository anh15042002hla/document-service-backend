package org.aibles.user.dto;

import lombok.Data;

@Data
public class UserDTO {
    private String id;
    private String email;
    private String name;
}
