package org.aibles.user.controller;

import lombok.RequiredArgsConstructor;
import org.aibles.document.dto.res.BasicDocumentResDto;
import org.aibles.user.service.UserService;
import org.aibles.util.paging.PagingReq;
import org.aibles.util.paging.PagingRes;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/me/reading-history")
    public ResponseEntity<PagingRes<BasicDocumentResDto>> listUserReadingHistory(@Validated() final PagingReq pagingReq){
        final Page<BasicDocumentResDto> documentResDtoPage = userService.listReadingHistory(pagingReq.makePageable());
        return ResponseEntity.ok(PagingRes.of(documentResDtoPage));
    }
}
