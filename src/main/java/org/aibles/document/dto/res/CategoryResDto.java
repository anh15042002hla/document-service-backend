package org.aibles.document.dto.res;

import lombok.Data;

@Data
public class CategoryResDto {
    private long id;
    private String name;
}
