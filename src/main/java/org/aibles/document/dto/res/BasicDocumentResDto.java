package org.aibles.document.dto.res;

import lombok.Data;

import java.util.Set;

@Data
public class BasicDocumentResDto {
    private long id;
    private String title;
    private String thumbS3ObjectKey;
    private Set<CategoryResDto> categories;
}
