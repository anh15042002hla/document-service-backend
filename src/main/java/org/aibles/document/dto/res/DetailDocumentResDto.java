package org.aibles.document.dto.res;

import lombok.Data;
import org.aibles.document.model.constants.DocumentType;

import java.time.Instant;

@Data
public class DetailDocumentResDto extends BasicDocumentResDto{
    private String author;
    private String fileS3ObjectKey;
    private DocumentType type;
    private Instant publicationDate;
    private long views;
}
