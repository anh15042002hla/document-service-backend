package org.aibles.document.repository;

import org.aibles.document.model.DocumentCategory;
import org.aibles.document.model.compositekey.DocumentCategoryKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentCategoryRepository extends JpaRepository<DocumentCategory, DocumentCategoryKey> {
}
