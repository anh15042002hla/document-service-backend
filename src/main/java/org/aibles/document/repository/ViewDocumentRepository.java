package org.aibles.document.repository;

import org.aibles.document.model.ViewDocument;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ViewDocumentRepository extends JpaRepository<ViewDocument, Long> {
    long countByDocumentId(final long documentId);
}
