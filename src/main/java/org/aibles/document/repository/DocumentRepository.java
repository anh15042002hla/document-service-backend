package org.aibles.document.repository;

import org.aibles.document.model.Document;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {

    Page<Document> findByDocumentCategoriesCategoryId(long categoryId, Pageable pageable);

    Page<Document> findByReadHistoriesUserId(String userId, Pageable pageable);
}
