package org.aibles.document.model.constants;

public enum DocumentType {
    WORD, PDF, EXCEL, POWERPOINT
}
