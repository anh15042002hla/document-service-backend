package org.aibles.document.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.aibles.comment.model.Comment;
import org.aibles.document.model.constants.DocumentType;
import org.aibles.user.model.ReadHistory;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "document")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Document implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "document_seq_generator")
    @SequenceGenerator(name = "document_seq_generator",
        sequenceName = "pk_document_id_seq",
        allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "author")
    private String author;

    @Column(name = "file_s3_object_key", nullable = false)
    private String fileS3ObjectKey;

    @Column(name = "thumb_s3_object_key")
    private String thumbS3ObjectKey;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private DocumentType type;

    @Column(name = "publication_date")
    private Instant publicationDate;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "document")
    private Set<DocumentCategory> documentCategories;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "document")
    private List<ViewDocument> viewDocuments;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "document")
    private List<ReadHistory> readHistories;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "document")
    private List<Comment> comments;

    public void setDocumentCategories(Set<DocumentCategory> documentCategories) {
        Optional.ofNullable(documentCategories)
                .ifPresent(dcs -> {
                    this.documentCategories = dcs;
                    this.getDocumentCategories().stream()
                            .map(documentCategory -> {
                                documentCategory.setDocument(this);
                                return documentCategory;
                            })
                            .collect(Collectors.toSet());
                });
    }

    public Set<Category> getCategories(){
        return this.documentCategories.stream()
                .map(DocumentCategory::getCategory).collect(Collectors.toSet());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Document)) return false;
        Document document = (Document) o;
        return Objects.equals(id, document.id);
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
