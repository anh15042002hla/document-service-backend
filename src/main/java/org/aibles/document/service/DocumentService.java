package org.aibles.document.service;

import org.aibles.document.dto.req.DocumentReqDto;
import org.aibles.document.dto.res.BasicDocumentResDto;
import org.aibles.document.dto.res.DetailDocumentResDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface DocumentService {

    DetailDocumentResDto createDocument(final DocumentReqDto documentReqDto);

    DetailDocumentResDto updateDocument(final long documentId, final DocumentReqDto documentReqDto);

    Page<BasicDocumentResDto> listDocuments(final Pageable pageable);

    DetailDocumentResDto deleteDocument(final long documentId);

    Page<BasicDocumentResDto> listDocumentsByCategory(final long categoryId, final Pageable pageable);

    DetailDocumentResDto getDocument(final long documentId);
}
