package org.aibles.document.controller.advice;

import lombok.extern.slf4j.Slf4j;
import org.aibles.util.exceptions.common.BusinessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@Slf4j
@RestControllerAdvice
public class ExceptionControllerAdvice {

    @ExceptionHandler({ ConstraintViolationException.class })
    public ResponseEntity<Object> validateErrorHandler(ConstraintViolationException ex) {
        Set<ConstraintViolation<?>> set = ex.getConstraintViolations();
        List<String> errorMessages = new ArrayList<>();
        for (Iterator<ConstraintViolation<?>> iterator = set.iterator(); iterator.hasNext();) {
            ConstraintViolation<?> next = iterator.next();
            errorMessages.add(next.getMessage());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessages);
    }

    @ExceptionHandler({BusinessException.class})
    public ResponseEntity<Object> applicationErrorHandler(BusinessException e) {
        log.error("Exception: errorCode: {}, Message: {}", e.getErr().getErrCode(), e.getErr().getMessage());
        return ResponseEntity.status(parseHttpStatus(e.getErr().getErrCode()))
                .body(e.getErr().getMessage());
    }

    private static HttpStatus parseHttpStatus(int errCode) {
        return HttpStatus.valueOf(errCode/1000);
    }
}
