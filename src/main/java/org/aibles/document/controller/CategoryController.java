package org.aibles.document.controller;

import lombok.RequiredArgsConstructor;
import org.aibles.document.dto.req.CategoryReqDto;
import org.aibles.document.dto.res.CategoryResDto;
import org.aibles.document.service.CategoryService;
import org.aibles.util.paging.PagingReq;
import org.aibles.util.paging.PagingRes;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/categories")
public class CategoryController {

    private final CategoryService categoryService;

    @PostMapping
    public ResponseEntity<CategoryResDto> createCategory(@Validated() @RequestBody CategoryReqDto categoryReqDto){
        final CategoryResDto categoryResDto = categoryService.createCategory(categoryReqDto);
        return ResponseEntity.ok(categoryResDto);
    }

    @GetMapping
    public ResponseEntity<PagingRes<CategoryResDto>> getCategories(@Validated()PagingReq pagingReq){
        final Page<CategoryResDto> categoryResDtoPage = categoryService.getCategories(pagingReq.makePageable());
        return ResponseEntity.ok(PagingRes.of(categoryResDtoPage));
    }
}
