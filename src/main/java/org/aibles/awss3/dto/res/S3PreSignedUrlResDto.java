package org.aibles.awss3.dto.res;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class S3PreSignedUrlResDto {
    private String presignedUrl;
    private String objectKey;

    public S3PreSignedUrlResDto(String presignedUrl, String objectKey){
        this.presignedUrl = presignedUrl;
        this.objectKey = objectKey;
    }
}
