package org.aibles.comment.service.iml;

import lombok.RequiredArgsConstructor;
import org.aibles.authentication.SecurityContextManager;
import org.aibles.comment.dto.req.CommentReqDto;
import org.aibles.comment.dto.res.CommentResDto;
import org.aibles.comment.model.Comment;
import org.aibles.comment.repository.CommentRepository;
import org.aibles.comment.service.CommentService;
import org.aibles.document.repository.DocumentRepository;
import org.aibles.util.component.MappingHelper;
import org.aibles.util.exceptions.DocumentNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CommentServiceIml implements CommentService {

    private final CommentRepository commentRepository;
    private final DocumentRepository documentRepository;
    private final MappingHelper mappingHelper;
    private final SecurityContextManager securityContextManager;

    @Override
    public CommentResDto createComment(final long documentId, final CommentReqDto commentReqDto) {
        Comment comment = mappingHelper.map(commentReqDto, Comment.class);

        documentRepository.findById(documentId)
                .ifPresentOrElse(doc -> {
                            comment.setDocument(doc);
                            comment.setUser(securityContextManager.getCurrentLoginUser());
                        },
                        () -> {throw new DocumentNotFoundException();}
                );
        return mappingHelper.map(commentRepository.save(comment), CommentResDto.class);
    }

    @Override
    public Page<CommentResDto> listDocumentComments(final long documentId, final Pageable pageable) {
        Page<Comment> documentCommentPage = commentRepository.findByDocumentId(documentId, pageable);
        return mappingHelper.mapPage(documentCommentPage, CommentResDto.class);
    }
}
