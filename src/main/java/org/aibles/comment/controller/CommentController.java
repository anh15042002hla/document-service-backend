package org.aibles.comment.controller;

import lombok.RequiredArgsConstructor;
import org.aibles.comment.dto.req.CommentReqDto;
import org.aibles.comment.dto.res.CommentResDto;
import org.aibles.comment.service.CommentService;
import org.aibles.util.paging.PagingReq;
import org.aibles.util.paging.PagingRes;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/documents")
@RequiredArgsConstructor
public class CommentController {

    private final CommentService commentService;

    @PostMapping("/{document_id}/comments")
    public ResponseEntity<CommentResDto> createComment(@PathVariable("document_id") final long documentId,
                                                       @Validated() @RequestBody final CommentReqDto commentReqDto){
        final CommentResDto commentResDto = commentService.createComment(documentId, commentReqDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(commentResDto);
    }

    @GetMapping("/{document_id}/comments")
    public ResponseEntity<PagingRes<CommentResDto>> listDocumentComments(@PathVariable("document_id") final long documentId,
                                                                         @Validated() final PagingReq pagingReq){
        final Page<CommentResDto> commentResDtoPage = commentService.listDocumentComments(documentId, pagingReq.makePageable());
        return ResponseEntity.ok(PagingRes.of(commentResDtoPage));
    }
}
