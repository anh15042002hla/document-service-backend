package org.aibles.comment.dto.res;

import lombok.Data;
import org.aibles.user.dto.UserDTO;

import java.time.Instant;

@Data
public class CommentResDto {
    private long id;
    private Long documentId;
    private String content;
    private Instant commentTime;
    private UserDTO user;
}
