package org.aibles.configuration;

import org.aibles.comment.dto.res.CommentResDto;
import org.aibles.comment.model.Comment;
import org.aibles.document.dto.res.BasicDocumentResDto;
import org.aibles.document.dto.res.DetailDocumentResDto;
import org.aibles.document.model.Document;
import org.aibles.document.model.DocumentCategory;
import org.aibles.util.component.MappingHelper;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.stream.Collectors;

@Configuration
public class ModelMapperConfig {

    public interface ModelMapperFactory {
        ModelMapper getMapper();
    }

    private ModelMapperFactory modelMapperFactory() {
        return () -> {
            ModelMapper mapper = new ModelMapper();
            mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

            mapper.typeMap(Document.class, BasicDocumentResDto.class)
                    .addMappings(mapping -> {
                        mapping.map(doc -> doc.getCategories(), BasicDocumentResDto::setCategories);
                    });

            mapper.typeMap(Document.class, DetailDocumentResDto.class)
                    .addMappings(mapping -> {
                        mapping.map(doc -> doc.getCategories(), DetailDocumentResDto::setCategories);
                    });

            mapper.typeMap(Comment.class, CommentResDto.class)
                    .addMappings(mapping -> {
                        mapping.map(comment -> comment.getDocumentId(), CommentResDto::setDocumentId);
                    });
            return mapper;
        };
    }

    @Bean
    public MappingHelper mappingHelper() {
        return new MappingHelper(modelMapperFactory());
    }
}
